
const domen = 'http://0.0.0.0:8010/api'

const v1 = '/v1'

const users = domen + v1 + '/users'

const game = '/game'

const psychics = '/psychics'

const rounds = domen + v1 + game + '/rounds'
const psychicsGuesses = domen + v1 + game + '/psychics_guesses'
const hiddenNumbers = domen + v1 + game + '/hidden_numbers'

export const getMeAPI = users + '/'
export const createUserAPI = users + '/'

export const createRoundAPI = rounds + '/'
export const checkAPI = psychicsGuesses + '/check'
export const getHiddenNumbersHistoryAPI = hiddenNumbers + '/history'
export const getPsychicGuessesHistoryAPI = psychicsGuesses + '/history'


export const getPsychicsAPI = domen + v1 + psychics + '/'
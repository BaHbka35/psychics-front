import { getPsychicGuessesHistoryAPI } from '../apis.js'


export async function getPsychicGuessesHistory(psychicId){
    try {

        const response = await fetch(getPsychicGuessesHistoryAPI+'?psychic_id='+psychicId, {
            method: 'GET',
            credentials: 'include'
        })
        const responseJson = await response.json()

        if (response.status >= 400) {
            alert(JSON.stringify(responseJson))
            return
        }
        return responseJson

    } catch (error) {
        console.log(error)
    }
}
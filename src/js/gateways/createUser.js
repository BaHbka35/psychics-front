import { createUserAPI } from "../apis.js"


export async function createUser(){
    try {

        const response = await fetch(createUserAPI, {
            method: 'POST',
            credentials: 'include'
        })
        const responseJson = await response.json()

        if (response.status >= 400) {
            alert(JSON.stringify(responseJson))
            return
        }

    } catch (error) {
        console.log(error)
    }

}
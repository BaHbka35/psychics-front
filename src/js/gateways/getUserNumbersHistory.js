import { getHiddenNumbersHistoryAPI } from '../apis.js'

export async function getUserNumbersHistory(){
    try {

        const response = await fetch(getHiddenNumbersHistoryAPI, {
            method: 'GET',
            credentials: 'include'
        })
        const responseJson = await response.json()

        if (response.status >= 400) {
            alert(JSON.stringify(responseJson))
            return
        }
        return responseJson

    } catch (error) {
        console.log(error)
    }
    
}
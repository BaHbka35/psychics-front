import { createRoundAPI } from "../apis.js"


export async function createRound(){
    try {

        const response = await fetch(createRoundAPI, {
            method: 'POST',
            credentials: 'include'
        })
        const responseJson = await response.json()

        if (response.status >= 400) {
            alert(JSON.stringify(responseJson))
            return
        }
        return responseJson

    } catch (error) {
        console.log(error)
    }

}
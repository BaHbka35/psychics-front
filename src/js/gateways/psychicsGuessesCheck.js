
import { checkAPI } from "../apis.js";


export async function checkPsychicsGuesses(number){
    try {

        const response = await fetch(checkAPI + `?user_hidden_number=${number}`, {
            method: 'POST',
            credentials: 'include'
        })
        const responseJson = await response.json()

        return responseJson

    } catch (error) {
        console.log(error)
    }

}
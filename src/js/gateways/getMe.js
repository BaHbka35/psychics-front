import { getMeAPI } from "../apis.js"
import { createUser } from "./createUser.js"


export async function getMe(){
    try {

        const response = await fetch(getMeAPI, {
            method: 'GET',
            credentials: 'include'
        })
        const responseJson = await response.json()
        console.log(responseJson)
        
        if (response.status == 404){
            await createUser()
        }
        if (response.status == 403){
            await createUser()
        }

    } catch (error) {
        console.log(error)
    }
}
import { getPsychicsAPI } from '../apis.js'


export async function getPsychics(){
    try {

        const response = await fetch(getPsychicsAPI, {
            method: 'GET',
            credentials: 'include'
        })
        const responseJson = await response.json()

        if (response.status >= 400) {
            alert(JSON.stringify(responseJson))
            return
        }
        return responseJson

    } catch (error) {
        console.log(error)
    }
}
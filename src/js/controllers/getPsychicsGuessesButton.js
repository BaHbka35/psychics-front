import { createRound } from "../gateways/getPsychicsGuesses.js"
import { psyhcicsGuessesPresenter } from "../presenters/psychicsGuessesList.js"

const getPsychicGuessesButtonNode = document.querySelector('.get-psychics-guesses-button')
const myNumberFormNode = document.querySelector('.my-number-form')
const getPsychicGuessesButtonWrapperNode = document.querySelector('.get-psychics-guesses-button-wrapper')


async function getPsychichsGuessesButtonController(event) {
    const psychicsGuesses = await createRound()
    psyhcicsGuessesPresenter(psychicsGuesses)
    getPsychicGuessesButtonWrapperNode.classList.add('disable')
    myNumberFormNode.classList.remove('disable')

}
getPsychicGuessesButtonNode.addEventListener('click', getPsychichsGuessesButtonController)
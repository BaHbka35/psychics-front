import { checkPsychicsGuesses } from '../gateways/psychicsGuessesCheck.js'


import { getUserNumbersHistory } from "../gateways/getUserNumbersHistory.js"
import { hiddenNumbersListPresentor } from "../presenters/userHiddenNumbersHistory.js"
import {psyhcicsInfoPresenter} from '../presenters/psychicsInfo.js'

import {showPsychicsGuessesHistory} from '../services/psychicsGuessesHidtory.js'

import {getPsychics} from '../gateways/getPsychics.js'

import { isInteger } from '../utils/isInteger.js'


const myNumberFormNode = document.querySelector('.my-number-form')
const myNumberNode = myNumberFormNode.querySelector('.my-number-lable')

const psychicsGuessesListNode = document.querySelector('.list-psychics-guesses')
const getPsychicGuessesButtonWrapperNode = document.querySelector('.get-psychics-guesses-button-wrapper')


async function myNumberFormController(event){
    event.preventDefault()

    let my_number = myNumberNode.value

    if (my_number < 10 || my_number > 99){
        alert('Нужно ввести положительное двузначное число')
        myNumberNode.value = ''
        return
    }
    if (!isInteger(my_number)){
        alert('То, что вы ввели - это не число')
        myNumberNode.value = ''
        return
    }

    const rightPsychics = await checkPsychicsGuesses(my_number)
    psyhcicsInfoPresenter(await getPsychics())
    for (const item of rightPsychics){
        const node = document.getElementById(`psychicGuess_${item.id}`)
        node.classList.add('highlight')
    }

    myNumberNode.value = ''

    getPsychicGuessesButtonWrapperNode.classList.remove('disable')
    psychicsGuessesListNode.classList.add('disable')
    myNumberFormNode.classList.add('disable')


    hiddenNumbersListPresentor(await getUserNumbersHistory())
    
    await showPsychicsGuessesHistory()
}


myNumberFormNode.addEventListener('submit', myNumberFormController)
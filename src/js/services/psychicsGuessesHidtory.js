import { getPsychics } from '../gateways/getPsychics.js'
import { getPsychicGuessesHistory } from '../gateways/getPsychicGuesseHistory.js'
import { psychicGuessesHistoryPresenter } from '../presenters/psychicsGuessesHistory.js'

const psychicsGuessesHistoryNode = document.querySelector('.psychics-guesses-history-wrapper')


export async function showPsychicsGuessesHistory(){

    psychicsGuessesHistoryNode.innerHTML = ''

    const psychics = await getPsychics()


    for (const psychic of psychics){
        let psychicGuessesHistory = await getPsychicGuessesHistory(psychic.id)
        psychicGuessesHistoryPresenter(psychic, psychicGuessesHistory)
    }

}
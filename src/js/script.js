import { getMe } from "./gateways/getMe.js";
import { getUserNumbersHistory } from "./gateways/getUserNumbersHistory.js"
import { hiddenNumbersListPresentor } from "./presenters/userHiddenNumbersHistory.js"
import { psyhcicsInfoPresenter } from './presenters/psychicsInfo.js'

import { getPsychics } from './gateways/getPsychics.js'

import { showPsychicsGuessesHistory } from "./services/psychicsGuessesHidtory.js";



await getMe()

hiddenNumbersListPresentor(await getUserNumbersHistory())
psyhcicsInfoPresenter(await getPsychics())

showPsychicsGuessesHistory()

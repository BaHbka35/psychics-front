export function isInteger(value) {
    return /^\d+$/.test(value);
}
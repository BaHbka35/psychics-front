const psychicsInfoListNode = document.querySelector('.psychics-info-list')


export function psyhcicsInfoPresenter(psychics){
    psychicsInfoListNode.innerHTML = ''

    for (const psychic of psychics){
        const psychicId = psychic.id
        const psychicName = psychic.name
        const psychicRating = psychic.rating

        const elem = `
            <li class='psychic-info-list-item' id=psychicGuess_${psychicId}>
                <p>Имя экстрасенса: ${psychicName}</p>
                <p>Рейтинг экстрасенса: ${psychicRating}</p>
            </li>
        `
        psychicsInfoListNode.innerHTML += elem
    }

}
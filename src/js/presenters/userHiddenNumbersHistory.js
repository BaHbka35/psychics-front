const hiddenNumbersListNode = document.querySelector('.user-numbers-history-list')


export function hiddenNumbersListPresentor(hiddenNumbers){

    hiddenNumbersListNode.innerHTML = ''

    for (const item of hiddenNumbers){
        const number = item.number

        const listElem = `
            <li class='user-numbers-history-list-item' >
                <p>Номер: ${number}</p>
            </li>
        `
        hiddenNumbersListNode.innerHTML += listElem
    }

}
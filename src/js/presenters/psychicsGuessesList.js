
const psychicsGuessesListNode = document.querySelector('.list-psychics-guesses')


export function psyhcicsGuessesPresenter(psyhcicsGuesses){

    psychicsGuessesListNode.innerHTML = ''

    for (const item of psyhcicsGuesses){
        const psychicName = item.psychic.name
        const psychicRating = item.psychic.rating
        const psychicId = item.psychic.id
        const number = item.number

        const listElem = `
            <li class='psychicGuessListItem' >
                <p>Имя экстрасенса: ${psychicName}</p>
                <p>Предполагаемый номер: ${number}</p>
            </li>
        `
                // <p>Рейтинг экстрасенса: ${psychicRating}</p>
        psychicsGuessesListNode.innerHTML += listElem
    }
    psychicsGuessesListNode.classList.remove('disable')
    
}
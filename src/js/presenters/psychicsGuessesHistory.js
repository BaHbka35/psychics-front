const psychicsGuessesHistoryNode = document.querySelector('.psychics-guesses-history-wrapper')


export function psychicGuessesHistoryPresenter(psychic, psychicGuessesHistory){

    const psychicName = psychic.name
    let psychicGuessesHistoryListNodes = ''

    for (const item of psychicGuessesHistory){
        psychicGuessesHistoryListNodes += 
                `<div class="psychic-guesses-history-list-item">${item.number}</div>`
    }

    const elem = `
        <div class="psychic-guesses-history-wrapper">
            <div class="psychic-name">${psychicName}</div>
            <div class="psychic-guesses-history-list-wrapper">
                <ul class="psychic-guesses-history-list">
                    ${psychicGuessesHistoryListNodes}
                </ul>
            </div>
        </div>
    `
    psychicsGuessesHistoryNode.innerHTML += elem

}

